# Littlefork

A modular pipeline for sequential batch processing. We use it for iterative
data retrievals and transformations.

## Installation

    npm install --save littlefork

## About

Littefork is a product of the
[Exposing the Invisible](https://exposingtheinvisible.org) team.

The best way to learn how to use it to follow the
[tutorial](docs/tutorial.md). If you rather want to develop plugins for
Littlefork, look at the [developers guide](docs/developers-guide.md) and the
[API guide](docs/api.md).

Littlefork is licensed under the [GPL3](LICENSE).

TODO:
  - Add contributing
  - Contact

## Benchmarks

To run the following benchmarks:

- `npm run bench:filter` - a custom implementation of `data.filter` and
  homonym.filter are replaced with lodash's `filter`, which is a blast
  compared to legacy.
- `npm run bench:equals` - a better performing version of `data.equals`, that
  is also in use for homonyms. Performs much better than legacy, especially
  when there are more elements to compare.

## Development

Please see the [contribution policy](Contributing.md) for more information.
