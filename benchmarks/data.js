import {curry, zip, every, sortBy, size, isEqual} from 'lodash/fp';
import {data as d} from '../lib';

const {equalsOne, dataId} = d;

export const legacyEquals = curry((xs, ys) => {
  const elems = zip(sortBy(dataId, xs), sortBy(dataId, ys));
  return isEqual(size(xs), size(ys)) &&
         every(([x, y]) => equalsOne(x, y), elems);
});
