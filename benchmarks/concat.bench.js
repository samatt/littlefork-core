import {map, merge} from 'lodash/fp';
import benchmark from 'benchmark';
import {data as d, utils} from '../lib';
import {data} from '../lib/test/generators';

const {dataId, concatOne, equalsOne} = d;
const {oldConcatManyWith, concatManyWith} = utils;

const dataOne = data(1);
const dataFifty = data(50);
const dataHundred = data(100);
const dataOneHashed = map(u => merge(u, {_lf_id_hash: dataId(u)}), dataOne);
const dataFiftyHashed = map(u => merge(u, {_lf_id_hash: dataId(u)}), dataFifty);
const dataHundredHashed = map(u => merge(u, {_lf_id_hash: dataId(u)}), dataHundred);

console.log('Finished creating the data'); // eslint-disable-line no-console

const suite = new benchmark.Suite();

suite
  .add('oldconcatManyWith 50/1', () =>
    oldConcatManyWith(equalsOne, concatOne, dataFifty, dataOne))
  .add('concatManyWith 50/1', () =>
    concatManyWith(dataId, concatOne, dataFifty, dataOne))
  .add('oldconcatManyWith hashed 50/1', () =>
    oldConcatManyWith(equalsOne, concatOne, dataFiftyHashed, dataOneHashed))
  .add('concatManyWith hashed 50/1', () =>
    concatManyWith(dataId, concatOne, dataFiftyHashed, dataOneHashed))

  .add('oldconcatManyWith 50/50', () =>
    oldConcatManyWith(equalsOne, concatOne, dataFifty, dataFifty))
  .add('concatManyWith 50/50', () =>
    concatManyWith(dataId, concatOne, dataFifty, dataFifty))
  .add('oldconcatManyWith hashed 50/50', () =>
    oldConcatManyWith(equalsOne, concatOne, dataFiftyHashed, dataFiftyHashed))
  .add('concatManyWith hashed 50/50', () =>
    concatManyWith(dataId, concatOne, dataFiftyHashed, dataFiftyHashed))

  .add('oldconcatManyWith 100/50', () =>
    oldConcatManyWith(equalsOne, concatOne, dataHundred, dataFifty))
  .add('concatManyWith 100/50', () =>
    concatManyWith(dataId, concatOne, dataHundred, dataFifty))
  .add('oldconcatManyWith hashed 100/50', () =>
    oldConcatManyWith(equalsOne, concatOne, dataHundredHashed, dataFiftyHashed))
  .add('concatManyWith hashed 100/50', () =>
    concatManyWith(dataId, concatOne, dataHundredHashed, dataFiftyHashed))

  .add('oldconcatManyWith 100/100', () =>
    oldConcatManyWith(equalsOne, concatOne, dataHundred, dataHundred))
  .add('concatManyWith 100/100', () =>
    concatManyWith(dataId, concatOne, dataHundred, dataHundred))
  .add('oldconcatManyWith hashed 100/100', () =>
    oldConcatManyWith(equalsOne, concatOne, dataHundredHashed, dataHundredHashed))
  .add('concatManyWith hashed 100/100', () =>
    concatManyWith(dataId, concatOne, dataHundredHashed, dataHundredHashed))

  .on('cycle', ev => console.log(String(ev.target))) // eslint-disable-line no-console
  .on('error', e => console.error(e.target.error)) // eslint-disable-line no-console
  .run();
