import {isEqual} from 'lodash/fp';
import benchmark from 'benchmark';
import {data as d} from '../lib';
import {equalsManyWith} from '../lib/utils';
import {data} from '../lib/test/generators';

const {identical, identicalOne} = d;

const dataOne = data(1);
const dataFifty = data(50);
const dataFiveHundred = data(500);
const dataFiveThousand = data(5000);

console.log('Finished creating the data'); // eslint-disable-line no-console

const identical2 = equalsManyWith(identicalOne);

const suite = new benchmark.Suite();

suite
  .add('identical current 1/1', () => identical(dataOne, dataOne))
  .add('identical 2 1/1', () => identical2(dataOne, dataOne))
  .add('identical lodash 1/1', () => isEqual(dataOne, dataOne))
  .add('identical current 50/1', () => identical(dataFifty, dataOne))
  .add('identical 2 50/1', () => identical2(dataFifty, dataOne))
  .add('identical lodash 50/1', () => isEqual(dataFifty, dataOne))
  .add('identical current 5000/500', () => identical(dataFiveThousand, dataFiveHundred))
  .add('identical 2 5000/500', () => identical2(dataFiveThousand, dataFiveHundred))
  .add('identical lodash 5000/500', () => isEqual(dataFiveThousand, dataFiveHundred))
  .add('identical current 5000/5000', () => identical(dataFiveThousand, dataFiveThousand))
  .add('identical 2 5000/5000', () => identical2(dataFiveThousand, dataFiveThousand))
  .add('identical lodash 5000/5000', () => isEqual(dataFiveThousand, dataFiveThousand))
  .on('cycle', ev => console.log(String(ev.target))) // eslint-disable-line no-console
  .on('error', e => console.error(e.target.error)) // eslint-disable-line no-console
  .run();
